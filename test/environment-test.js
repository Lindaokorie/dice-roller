import chai,{expect} from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import Environment from '../app/src/environment';

chai.use(sinonChai);

describe('Environment', () => {
    
    var sandbox = sinon.createSandbox();
    //These methods work with the DOM
    sandbox.stub(Environment.prototype, 'createRenderer').returns({});
    sandbox.stub(Environment.prototype, 'createCamera').returns({});

    //These methods have nothing to do with the DOM
    sandbox.spy(Environment.prototype, 'createScene');
    sandbox.spy(Environment.prototype, 'createWorld');
    let E = new Environment();
    
    after( () => {
        sandbox.restore();
    });
    
    context('creating an instance of the class', () => {
        it('should create and return a renderer', () => {
            expect(E.createRenderer).to.have.been.calledOnce;
            expect(typeof E.renderer).to.eql("object");
            
        });


        it('should create and return a scene', () => {
           expect(E.createScene).to.have.been.calledOnce;
           expect(typeof E.scene).to.eql("object");
        });


        it('should create and return a camera', () => {
            expect(E.createCamera).to.have.been.calledOnce;
            expect(typeof E.camera).to.eql("object");
        });


        it('should create and return a world', () => {
            expect(E.createWorld).to.have.been.calledOnce;
            expect(typeof E.world).to.eql("object");
        });

        
    });

    context('adding a dice to the scene and world', () => {
        let dice = {
                boxBody: {},
                mesh: {}
            };
        let addDiceSpy = sandbox.spy(Environment.prototype, 'addDice');
        let worldStub = sandbox.spy(E.world,'add');
        E.addDice(dice);

        it('should be called with a dice with the boxBody and mesh parameters', () => {
            expect(dice).to.have.property('boxBody');
            expect(dice).to.have.property('mesh');
        });

        it('should be called with a dice parameter', () => {
            expect(addDiceSpy).to.have.been.calledWith(dice);

        });

        it('should add the dice to the scene and the world', () => {
            expect(worldStub).to.have.been.called;
            
        })
    })
})
