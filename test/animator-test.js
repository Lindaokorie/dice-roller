import chai,{expect} from 'chai';
import Animator from '../app/src/animator';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
chai.use(sinonChai);


describe("Animator",() => {
    
    let renderer = {}, scene = {}, camera = {}, world ={}, dice = {};
    let sandbox = sinon.createSandbox()
    let spy = sandbox.spy(Animator);
    let obj = new spy(renderer,scene,camera,world,dice);

    after( () => {
        sandbox.restore();
    });

    context("creating an instance of the class", () => {
        it('should be able to create an instance of the class', () => {
            expect(obj).to.be.an.instanceOf(Animator);
        });
        
        it('should be called with objects', () => {
            expect(spy).to.have.been.calledWithExactly(renderer,scene,camera,world,dice);
        });
        
        
    });

    context("#animate()", () => {
    
        let frameStub = sandbox.stub(Animator.prototype,'animate').callsFake(function reqAnimationFrame(ani){
            return ani;
        });
        obj.animate();

        it('should call the requestAnimationFrame method', () => {
            expect(frameStub).to.have.been.called;
        });
    });
});
