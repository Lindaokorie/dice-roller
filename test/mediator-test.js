import chai,{expect} from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import Mediator from '../shared/server-db-mediator';
// import Mediator from '../shared/mediator';

chai.use(sinonChai);


describe('Mediator', () => {
    context('calling the mediator constructor', () => {
        it('should be called with a parameter', () => {
            let sandbox = sinon.createSandbox()
            let spy = sandbox.spy(Mediator);
            let mediator = new spy(db);
            expect(spy).to.have.been.calledWithExactly(db);
        })
    })

    context('calling the "createGame" function', () => {
        it('should create a Player', () => {
            let Player= sinon.spy(); //socket.emit
            expect(Player).to.be.called;
        });

        it('should make the database call the insert method', () => {
            expect(db.insert).to.be.called;
        });

        it('should make the database call the insert method with parameters', () => {
            expect(db.insert).to.be.calledWithExactly();
        });
    })

    context('calling the "joinGame" function', () => {
        it('should create a Player', () => {
            let Player= sinon.spy(); //socket.emit
            expect(Player).to.be.called;
        });

        it('should make the database call the update method', () => {
            expect(db.update).to.be.called;
        });

        it('should make the database call the insert method with parameters', () => {
            expect(db.update).to.be.calledWithExactly();
        });
    });

    context('calling the "sendStatus" function', () => {
        it('should emit a status event', () => {
            let socket= sinon.spy(); //socket.emit
            expect(socket).to.be.calledWith('status');
        });
    });

});