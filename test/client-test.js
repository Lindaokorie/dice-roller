import chai,{expect} from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
// import Client from '../client/src/client';

chai.use(sinonChai);

describe('Client', () => {
    describe('Creating a Game', () => {
        context('calling the "callGame" function', () => {
            it('should emit a "makeGame" event', () => {
                let socket= sinon.spy(); //socket.emit
                expect(socket).to.be.calledWith('makeGeme');
            });
        });
    });

    describe('Joining a Game', () => {
        context('calling the "addToGame" function', () => {
            it('should emit a "joinGame" event', () => {
                let socket = sinon.spy();
                expect(socket).to.be.calledWith('joinGame');
            });
        });
    });

    describe('Setting the Status', () => {
        context('calling the "setStatus" function', () => {
            it('should be passed with a parameter', () =>{
                let setStatus = sinon.spy();
                expect(setStatus).to.be.calledWith();
            })
        })
    })
});

