const path = require('path');


const docsBuild = {
	entry: {
		'app/dist/lib/dice-roller': './app/src/game-assembler.js',
		'app/dist/lib/start-screen':'./app/src/index.js'
	},
	output: {
		path: path.join(__dirname, ''),
		filename: '[name].js'
	}
};

module.exports = [docsBuild];