(function(){
    let username = document.getElementById('username');
    let players = document.getElementById('game_size');
    let status = document.getElementById('status');
    let createBtn =  document.getElementById('createBtn');
    let statusDefault = status.textContent;


    let setStatus = (s) => {
        status.textContent = s;
        if(s !== statusDefault){
            setTimeout(function(){
                setStatus(statusDefault);
            },4000);
        }
    }

    //Connect to socket.io
    let socket = io();
    
    //Check for connection
    if(socket !== undefined){
        console.log('Connected to socket...');

        socket.on('status', function(data){
            setStatus((typeof data === 'object')? data.message : data);
        });

        //Handle input
        createBtn.addEventListener('click',function(event){
            socket.emit('Make Game',{name: username.value,count: players.value});
            window.location.href = "game-start.html"
            event.preventDefault();
        });
    }
})();  