import Animator from './animator';
import Environment from './environment';
import Dice from './dice';

function start(){
    let environment = new Environment();
    let dice1 = new Dice();
    let dice2 = new Dice();
    let game = new Animator(environment.renderer, environment.scene, environment.camera, environment.world, [dice1, dice2]);

    
    dice1.createDice(Math.random()*15, Math.random()*10, Math.random()*10); 
    dice2.createDice(Math.random()*15, Math.random()*10, Math.random()*10);
    environment.addDice([dice1, dice2]);
    game.animate();
}
start();