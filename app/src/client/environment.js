import {Scene, WebGLRenderer, PerspectiveCamera, Vector3} from 'three';
import {Body,Plane,NaiveBroadphase,Material,World,Vec3} from 'cannon';

class Environment{
    constructor(){
        this.renderer = this.createRenderer();
        this.camera = this.createCamera();
        this.scene = this.createScene();
        this.world = this.createWorld();
    }
    createRenderer(){
        let renderer = new WebGLRenderer();
        renderer.setSize(window.innerWidth, window.innerHeight);
        let canvas = document.getElementById("canvas");
        canvas.appendChild(renderer.domElement);
        return renderer;
    }

    createScene(){
        let scene = new Scene();
        return scene;
    }

    createCamera(){
        let camera = new PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000 );
        camera.position.set(20,20,20);
        camera.lookAt( new Vector3() );
        return camera;
    }

    createWorld(){
        let world = new World();
        world.broadphase= new NaiveBroadphase();
	    world.gravity.set( 0 , -15, 0 );
        const worldMaterial= new Material();

        //creating surfaces for the shapes to land on and bounce off
	    let plane = new Plane();
	    let groundbody = new Body( {mass : 0, shape : plane, material : worldMaterial} );
	    groundbody.quaternion.setFromAxisAngle( new Vec3(1, 0, 0), - Math.PI / 2 );
        world.add( groundbody );
        
	    let leftwallbody = new Body( {mass: 0, shape : plane, material : worldMaterial} );
	    leftwallbody.quaternion.setFromAxisAngle( new Vec3(0, 1, 0), -Math.PI / 2 );
	    leftwallbody.position.set(10,0,0);
	    world.add( leftwallbody );

	    let rightwallbody = new Body( {mass: 0, shape : plane, material : worldMaterial} );
	    rightwallbody.quaternion.setFromAxisAngle( new Vec3(0, 1, 0), Math.PI / 2 );
	    rightwallbody.position.set(-10, 0, 0);
        world.add( rightwallbody );
        return world;
    }

    addDice(dice){
        for(let i = 0; i < dice.length; i++){
            this.world.add(dice[i].boxBody); 
            this.scene.add(dice[i].mesh);
        }
    }
} 

export default Environment; 