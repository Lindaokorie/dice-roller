(function(){
    
    let code = document.getElementById('gameCode').value;
    let user = document.getElementById('username').value;
    let joinBtn = document.getElementById('joinBtn');

    let setStatus = (s) => {
        status.textContent = s;
        if(s !== statusDefault){
            setTimeout(function(){
                setStatus(statusDefault);
            },4000);
        }
    }

    //Connect to socket.io
    let socket = io();
    //Check for connection
    if(socket !== undefined){
        console.log('Connected to socket...');

        socket.on('status', function(data){
            setStatus((typeof data === 'object')? data.message : data);
        });
    
        joinBtn.addEventListener('click', function(event){
            socket.emit('Join Game',{code:code,name: user});
	        event.preventDefault();
        });
    }
})();
