class Animator{
	constructor(renderer,scene,camera,world,dice){
		this.renderer = renderer;
		this.scene = scene;
		this.camera = camera;
		this.world = world;
		this.dice = dice;
	}


	animate(){
		requestAnimationFrame(() => this.animate());
    	this.world.step( 1 / 60 );
		for(var i = 0; i < this.dice.length; i++){
			this.joinMeshandBody(this.dice[i].mesh, this.dice[i].boxBody);
		}
		
    	this.renderer.render(this.scene,this.camera);
	};
	

	joinMeshandBody(mesh, body){
		//joins the contents of the scene and the world together to form objects.
		mesh.position.copy(body.position);
		mesh.quaternion.copy(body.quaternion);
	};
}
export default Animator;




