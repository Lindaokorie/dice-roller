import {Material, Body, Box, Vec3} from "cannon";
import {BoxGeometry, Mesh, MeshBasicMaterial, TextureLoader} from "three";

class Dice{
    constructor(){
        let halfextents = new Vec3(1, 1, 1);
		this.halfextents = halfextents;
		let loader = new TextureLoader();

		this.geometry = new BoxGeometry(halfextents.x*2, halfextents.y*2, halfextents.z*2);
		this.materialArray = [
			new MeshBasicMaterial({map: loader.load('../images/die-face-1.png')}),
			new MeshBasicMaterial({map: loader.load('../images/die-face-6.jpeg')}),
			new MeshBasicMaterial({map: loader.load('../images/die-face-2.jpeg')}),
			new MeshBasicMaterial({map: loader.load('../images/die-face-5.jpeg')}),
			new MeshBasicMaterial({map: loader.load('../images/die-face-3.jpeg')}),
            new MeshBasicMaterial({map: loader.load('../images/die-face-4.png')})
                        ];
        
    }

    createDice(x, y, z) {
        let boxShape = new Box(this.halfextents); 
        let boxMaterial = new Material();
        let boxBody = new Body( { mass : 0.4, shape : boxShape, material : boxMaterial } );
        let mesh = new Mesh(this.geometry, this.materialArray);
        this.boxBody = boxBody;
        boxBody.position.set(x, y, z);
        this.mesh = mesh;
        
    }

    
} 
export default Dice;
