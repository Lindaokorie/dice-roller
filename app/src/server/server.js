const mongo = require('mongodb').MongoClient;
const express = require("express");
const app = express();
const server = app.listen(4000, () =>{
    console.log('Server running on port 4000.');
});
const io = require('socket.io').listen(server);
const Player = require('./app/src/player');

app.use(express.static('app'));



//Connect to mongodb
mongo.connect('mongodb://127.0.0.1/dice-roller',{ useNewUrlParser: true }, function(err,client){
    if(err){
        throw err;
    }
    console.log('Database connected...');
    const db = client.db('diceGame');

    //Connect to socket.io
    io.on('connection', function(socket){
        let game = db.collection('game');
        if(game){
            console.log('Collection created');
        }

        
        sendStatus = (stat) => {
            socket.emit('status',stat);
        }

        // Generates a game code when called.
        codeGenerator = () =>{
            var text = "";
            var charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

            for(var i= 0; i < 5; i++){
                text += charset.charAt(Math.floor(Math.random() * charset.length));
            }
            return text;
        }

        //when a user tries to create a game 
        socket.on('Make Game', function(data){
            let name = data.name;
            let game_size = data.game_size;
            let gameCode = codeGenerator();


            if(name == ''){
                sendStatus('Please enter a valid username.');
            }
            else{
                let player = new Player(name);
                socket.join(gameCode);
                console.log('joined ' + gameCode);
                game.insert({ GameCode: gameCode,GameSize: game_size, Players:[player] });
                socket.emit('showCode',gameCode);
                
            }
        });

        //when a user tries to join a game
        socket.on('Join Game', function(data){
            let user = data.name;
            let code = data.code;

            if(user == ''){
                sendStatus('Please enter a valid name you would like to bear.');
            }
            else{
                let id = db.findOne({GameCode:code}).Players.length;
		        let max_players = db.findOne({GameCode:code}).GameSize;
		
		        if(id < max_players && id != 0){
                    let new_player = new Player(user);
                    socket.join(code);
                    game.update({GameCode:code},{$push: { Players: new_player} });
                    sendStatus({ message: 'Player Added'});
		        }
		        else {
			        sendStatus("So Sorry. The game is full.");
		        }
            }
        });

        //when a user tries to end a game
        socket.on('Leave Game', function (){
            
        })
        
    }); //end of io.on(connection)
});// end of connection to mongodb
