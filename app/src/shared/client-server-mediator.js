class ClientServerMediator {

    callGame(user, players){
        socket.emit('Make Game',{name: user,count: players});
    }

    connectToGame(code, user){
        socket.emit('Join Game',{code:code,name: user});
    }

    getGameCode(code){
        let game_code = document.getElementById('gameCode');
        game_code.innerHTML = code;
    }

    sendStatus(stat) {
        socket.emit('status',stat);
    }
}
export default ClientServerMediator;