class ServerDbMediator {
	constructor(db){
		this.db = db;
	}

	//Server functions
	createGame(code, name, size) {
		let player = new Player(name);
		db.insert({ GameCode: code,GameSize: size, Players:[player] });
  }

	joinGame(code, user) {
		let id = db.findOne({GameCode:code}).Players.length;
		let max_players = db.findOne({GameCode:code}).GameSize;
		
		if(id < max_players && id != 0){
			let new_player = new Player(user);
            db.update({GameCode:code},{$push: { Players: new_player} });
		}
		else {
			sendStatus("So Sorry. The game is full.");
		}
	}		
}
export default ServerDbMediator;
